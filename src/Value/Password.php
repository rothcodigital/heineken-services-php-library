<?php namespace Heineken\Value;

use \Heineken\Value\ValueObject as ValueObject;
use \Exception;

class Password extends ValueObject
{
    /**
     * Set the value.
     *
     * @param string $value
     * @return self
     */
    public function setValue($value)
    {
        $this->hasMinCharacters($value, 1);
        $this->hasMaxCharacters($value, 64);
        
        parent::setValue($value);
    }
}
