<?php namespace Heineken\WebService\AGWaaS;

use \Heineken\WebService\Service as Service;

class LegalDrinkingAgeService extends Service
{
    /**
     * @var string
     */
    protected $scope = 'http://agegateway-services.heineken.com';

    /**
     * @var string
     */
    protected $serviceUrl = 'https://agwaas.heineken.com/LegalDrinkingAgeService.svc/v1/';

    /**
     * @var String
     */
    protected $method = 'post';

    /**
     * @var Array
     */
    protected $methodsUrlExts = [
            'AgeCheck' => ''
        ];
    

}