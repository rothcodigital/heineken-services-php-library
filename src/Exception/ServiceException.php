<?php
namespace GuzzleHttp\Exception\ClientException;

/**
 * Exception when a client error is encountered (4xx codes)
 */
class ServiceException extends ClientException {}
