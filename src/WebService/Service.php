<?php namespace Heineken\WebService;

use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Stream\Stream as Stream;
use GuzzleHttp\Exception\ClientException as ClientException;

use Heineken\WebService\Authorize as Authorize;
use Heineken\Aggregate\Account as Account;
use Heineken\Value as Value;

class Service
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    protected $serviceUrl = 'https://craas-v2.heineken.com/';

    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * @var String
     */
    protected $method = 'get';

    /**
     * @var \Heineken\WebService\Authorize
     */
    protected $auth;

    /**
     * @var \Heineken\Value\Token
     */
    protected $token;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    /**
     * Create the service object.
     */
    public function __construct($username,$password)
    {

        $this->username = $username;
        $this->password = $password;

        $this->client = new Guzzle();
        $this->url = $this->getServiceUrl();

        $this->auth = new Authorize($this->getAccount());
        $this->auth->setUrl($this->getACSHeader());

        $this->token = $this->auth->getToken();

    }

    private function getACSHeader(){

        try {

            $response = $this->client->get($this->getUrl());

            return false;

        } catch (ClientException $e) {

            return $e->getResponse()->getHeader('ACSUrl')[0];

        }

    }

    public function request($urlData = [], $bodyData = []){

        if($this->validateData($urlData)){

            $response = $this->client->{$this->method}($this->buildUrl($urlData),[
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'WRAP access_token="'.$this->token->getValue().'"'
                ],
                'body' => json_encode($bodyData)
            ]);

            return $response;

        }

    }

    private function getAccount(){

        if(isset($this->scope) && isset($this->username) && isset($this->password)){

            return new Account($this->getUsername(),$this->getPassword(),$this->getScope());

        }

        return false;

    }

    private function validateData($data){

        if( !array_key_exists($data['method'], $this->methodsUrlExts) ){

            throw new \Exception('A valid "method" value was not supplied.');

        } else if( strpos($this->buildUrl($data),'{') !== false ){

            throw new \Exception('The required data for the API method: "'.$data['method'].'" was not passed.');
        
        }

        return true;

    }

    private function buildUrl($params){

        $finalUrl = $this->getUrl().$this->getMethodExt($params['method']);

        foreach ($params as $key => $value) {
            $finalUrl = str_replace('{'.$key.'}', $value, $finalUrl);
        }

        return $finalUrl;

    }

    private function getServiceUrl(){

        return $this->serviceUrl;

    }

    private function getUrl(){

        return $this->url;

    }

    private function getMethodExt($method){

        return $this->methodsUrlExts[$method];

    }

    private function setUrl($url){

        return $this->url = $url;

    }

    private function getScope(){

        return new Value\Scope($this->scope);

    }

    private function getUsername(){

        return new Value\Username($this->username);

    }

    private function getPassword(){

        return new Value\Password($this->password);

    }

}
