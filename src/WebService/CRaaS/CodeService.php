<?php namespace Heineken\WebService\CRaaS;

use \Heineken\WebService\Service as Service;

class CodeService extends Service
{
	/**
     * @var string
     */
    protected $scope = 'http://craas.heineken.com';

    /**
     * @var string
     */
    protected $serviceUrl = 'https://craas-v3.heineken.com/CodeService.svc/v1/';

    /**
     * @var Array
     */
    protected $methodsUrlExts = [
    		'ValidateCode' 			=> 'validate/{campaignId}/{code}/{markCodeAsUsed}',
    		'GetNonPurchaseCode' 	=> 'nonpurchase/{campaignId}/{emailAddress}/{magicNumber}'
    	];
    

}
