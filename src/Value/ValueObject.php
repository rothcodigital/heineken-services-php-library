<?php namespace Heineken\Value;

class ValueObject
{
    /** 
     * @var mixed
     **/
    protected $value;
    
    /**
     * Create a new value object
     *
     * @param mixex $value
     */
    public function __construct($value)
    {
        if (is_string($value)) {
            $value = trim($value);
        }
        
        $this->setValue($value);
    }
    
    /**
     * Set the value.
     *
     * @param mixed $value
     */
    public function setValue($value)
    {
        return $this->value = $value;
    }
    
    /**
     * Return the value.
     *
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * Return the value as a string.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getValue();
    }
    
    protected function hasMinCharacters($value, $num)
    {
        if (strlen($value) <= $num) {
            throw new \Exception(
                sprintf(
                    'Username does not have enough characters. Minimum of %s required.',
                    $num
                )
            );
        }
        
        return true;
    }
    
    protected function hasMaxCharacters($value, $num)
    {
        if (strlen($value) >= $num) {
            throw new \Exception(
                sprintf(
                    'Username has too many characters. Maximum of %s required.',
                    $num
                )
            );
        }
        
        return true;
    }
}