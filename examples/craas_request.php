<?php

require('../vendor/autoload.php');

use Heineken\WebService\CRaaS as CRaaS;

// This information is supplied by Heineken WebCentre
$credentials = [
    'username' => '',
    'password' => '',
    'campaignId' => ''
];

$CRaaS = new CRaaS\CodeService($credentials['username'], $credentials['password']);

$response = $CRaaS->request([
    'method' => 'GetNonPurchaseCode',
    'campaignId' => $credentials['campaignId'],
    "emailAddress" => 'FALSE',
    "magicNumber" => 'FALSE'
]);


header('Content-Type: application/json');
echo $response->getBody();