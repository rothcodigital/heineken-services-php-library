<?php namespace Heineken\WebService\CRaaS;

use \Heineken\WebService\Service as Service;

class PrizeService extends Service
{
	/**
     * @var string
     */
    protected $scope = 'http://craas.heineken.com';

    /**
     * @var string
     */
    protected $serviceUrl = 'https://craas-v3.heineken.com/PrizeService.svc/v1/';

    /**
     * @var Array
     */
    protected $methodsUrlExts = [
        'RedeemCode'                => 'redeem/{campaignId}/{code}',
        'GetAllPrizesForCampaign'   => 'allprizes/{campaignId}',
        'ClaimPrize' 			    => 'claim/{campaignId}/{code}/{prizeId}'
    ];


}
