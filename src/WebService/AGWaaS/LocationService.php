<?php namespace Heineken\WebService\AGWaaS;

use \Heineken\WebService\Service as Service;

class LocationService extends Service
{
    /**
     * @var string
     */
    protected $scope = 'http://agegateway-services.heineken.com';

    /**
     * @var string
     */
    protected $serviceUrl = 'https://agwaas.heineken.com/LocationService.svc/v1/';

    /**
     * @var Array
     */
    protected $methodsUrlExts = [
            'ForceIso' => 'ForceIso/{isoCode}',
            'IP'       => '{ip}'
        ];
    

}