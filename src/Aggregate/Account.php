<?php namespace Heineken\Aggregate;

use Heineken\Value\Username;
use Heineken\Value\Password;
use Heineken\Value\Scope;

class Account
{
    /**
     * @var Username
     */
    protected $username;
    
    /**
     * @var Password
     */
    protected $password;
    
    /**
     * @var Scope
     */
    protected $scope;
    
    /**
     * Create the service object. 
     *
     * @param Username $username
     * @param Password $password
     * @param Scope $scope
     */
    public function __construct(Username $username, Password $password, Scope $scope)
    {
        $this->setUsername($username);
        $this->setPassword($password);
        $this->setScope($scope);
    }
    
    /**
     * Set the username.
     *
     * @param Username $username
     * @return self
     */
    public function setUsername(Username $username)
    {
        $this->username = $username;
    }
    
    /**
     * Get the username value object.
     * 
     * @return Username
     */
    public function getUsername()
    {
        return $this->username;
    }
    
    /**
     * Set the password.
     *
     * @param Password $$password
     * @return self
     */
    public function setPassword(Password $password)
    {
        $this->password = $password;
    }
    
    /**
     * Get the password value object.
     * 
     * @return Password
     */
    public function getPassword()
    {
        return $this->password;
    }
    
    /**
     * Set the scope.
     *
     * @param Scope $scope
     * @return self
     */
    public function setScope(Scope $scope)
    {
        $this->scope = $scope;
    }

    
    /**
     * Get the scope value object.
     * 
     * @return Scope
     */
    public function getScope()
    {
        return $this->scope;
    }
    
    /**
     * Convert the aggregate to an array.
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'wrap_username' => $this->getUsername()->getValue(),
            'wrap_password' => $this->getPassword()->getValue(),
            'wrap_scope'    => $this->getScope()->getValue(),
        );
    }
}
