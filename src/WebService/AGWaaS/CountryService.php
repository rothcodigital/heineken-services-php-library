<?php namespace Heineken\WebService\AGWaaS;

use \Heineken\WebService\Service as Service;

class CountryService extends Service
{
    /**
     * @var string
     */
    protected $scope = 'http://agegateway-services.heineken.com';

    /**
     * @var string
     */
    protected $serviceUrl = 'https://agwaas.heineken.com/CountryService.svc/v1/';

    /**
     * @var Array
     */
    protected $methodsUrlExts = [
            'GetCountryList' => '{language}'
        ];
    

}