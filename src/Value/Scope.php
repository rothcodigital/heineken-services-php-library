<?php namespace Heineken\Value;


use \Exception;

use Heineken\Value\ValueObject;

class Scope extends ValueObject
{
    /**
     * Set the value.
     *
     * @param string $value
     * @return self
     */
    public function setValue($value)
    {
        $this->uri = new \Heineken\Value\Uri($value . '/');
        
        $this->isValidUri($value);
        $this->hasNoQueryParameters($value);
        $this->hasNoAnchors($value);
        $this->hasPathSegmentsLessThanOrEqualTo($value, 32);
        $this->hasMaxCharacters($value, 256);
        $this->isUrlEncoded($value);
        
        parent::setValue($value);
    }
    
    protected function isValidUri($value)
    {
        if (!$this->uri->isValid()) {
            throw new \InvalidArgumentException('Not a valid URI.');
        }
    }
    
    protected function hasNoQueryParameters($value)
    {
        if (!empty($this->uri->query)) {
            throw new \InvalidArgumentException('No query parameters allowed.');
        }
    }
    
    protected function hasNoAnchors($value)
    {
        if (!empty($this->uri->fragment)) {
            throw new \InvalidArgumentException('No anchor parameters allowed.');
        }
    }
    
    protected function hasPathSegmentsLessThanOrEqualTo($value, $num)
    {
        $segments = explode('/', $this->uri->path);
        
        if (count($segments) > $num) {
            throw new \InvalidArgumentException(sprintf('Only %s path segments allowed.', $num));
        }
    }
    
    protected function isUrlEncoded($value)
    {
        return true;
    }
}
