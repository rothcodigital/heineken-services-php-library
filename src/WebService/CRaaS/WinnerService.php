<?php namespace Heineken\WebService\CRaaS;

use \Heineken\WebService\Service as Service;

class WinnerService extends Service
{
	/**
     * @var string
     */
    protected $scope = 'http://craas.heineken.com';

    /**
     * @var string
     */
    protected $serviceUrl = 'https://craas-v3.heineken.com/WinnerService.svc/v1/';

    /**
     * @var string
     */
    protected $method = 'put';

    /**
     * @var Array
     */
    protected $methodsUrlExts = [
    		'store' => ''
    	];
    

}
