<?php namespace Heineken\WebService;

use GuzzleHttp\Client as Guzzle;

use \Heineken\Aggregate\Account;
use \Heineken\Value\Token;

class Authorize
{
    /**
     * @var string
     */
    protected $url;

    /**
     * @var RequestInterface
     */
    protected $client;

    /**
     * @var Account
     */
    protected $account;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var int
     */
    protected $ttl;

    /**
     * Create a new acs instance service.
     *
     * @param RequestInterface $client
     * @param Account $account
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    protected function requestToken(){

        $guzzle = new Guzzle();

        $response = $guzzle->request('POST', $this->buildUrl(),[
            'headers'       => [
                    'Content-Type', 'application/x-www-form-urlencoded'
                ],
            'form_params'   => [
                    'wrap_name'     => $this->account->getUsername()->getValue(),
                    'wrap_password' => $this->account->getPassword()->getValue(),
                    'wrap_scope'    => $this->account->getScope()->getValue()
                ]
            ]);

        return $response;

    }

    protected function getTokenData(){

        $response = $this->requestToken();
        $responseBody = $this->parse($response->getBody());

        $this->token = urldecode($responseBody['wrap_access_token']);
        $this->ttl   = (int) $responseBody['wrap_access_token_expires_in'];

    }


    /**
     * Parse the response body string.
     *
     * @param string $response
     * @return array
     */
    protected function parse($response)
    {
    	//First remove any extraneous header information from the returned POST variables
    	$pos = strpos($response, 'wrap_access_token=');

    	if ($pos === false) {
    		$pos = strpos($response, 'wrap_error_reason=');
    	}
    	$codes = '?' . substr($response, $pos, strlen($response));

    	//RegEx the string to separate out the variables and their values
    	if (preg_match_all('/[?&]([^&=]+)=([^&=]+)/', $codes, $matches)) {
    		for($i =0; $i < count($matches[1]); $i++) {
    			//The first element in the matches array is the combination
    			//of both matches.
    			$contents[$matches[1][$i]] = $matches[2][$i];
    		}
    	} else {
    		throw new \Exception('No matches for regular expression.');
    	}

    	return $contents;
    }

    public function buildUrl()
    {
        return 'https://'.$this->getUrl().'/WRAPv0.9/';
    }


    /**
     * Set the Authorization URL.
     *
     * @param string $url
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * Get the Authorization URL.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get the Authorization Token.
     *
     * @return \Heineken\Age\Value\Token
     */
    public function getToken()
    {
        if(!isset($this->token)){
            $this->getTokenData();
        }

        return new Token($this->token);
    }

    /**
     * Get the Authorization Token time to live period in seconds.
     *
     * @return int
     */
    public function getTtl()
    {
        return $this->ttl;
    }
}
