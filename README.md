# Heineken Web Services PHP

The purpose of this PHP library is to provide easy access to some of Heineken's web services. The structure of the code and naming of the classes, variables and methods included in this library are based on the .NET documentation provided by Heineken Webcentre.

Please make sure you have a valid username and password for the Heineken webservice you are using. If you do not have this please contact Heineken Webcentre (webcentre@heineken.com).

###Dependencies
- react/promise
- guzzlehttp/streams
- guzzlehttp/guzzle
- guzzlehttp/webservices

### Installation using Composer

##### Step 1
Update your projects 'composer.json' file to require the 'heineken/webservices' package

    "require": {
        "heineken/webservices": "dev-master"
    }
    
##### Step 2 
Include the url to this repository

    "repositories" : [
        {
            "type": "vcs",
            "url" : "git@bitbucket.org:rothcodigital/heineken-services-php-library.git"
        }
    ]
    
##### Step 3
Run the composer install:

    $ composer install



### Connecting to a webservice

##### GET request
Sending data via url example:

    <?php
    
        include('vendor/autoload.php');
        use Heineken\WebService\CRaaS as CRaaS;

        $CRaaS = new CRaaS\CodeService('USERNAME', 'PASSWORD');

        $response = $CRaaS->request([
            "method"         => "ValidateCode",
            "campaignId"     => "12345",
            "code"           => "XXXXXXXX",
            "markCodeAsUsed" => "FALSE"
        ]);

        var_dump(json_decode($response->getBody(),true));
        
    ?>
    
Will output:

    array(2) { ["CodeStatus"]=> string(0) "" ["IsCodeValid"]=> bool(false) }
    
##### PUT/POST request
Sending data via request body example:

    <?php
    
        include('vendor/autoload.php');
        use Heineken\WebService\CRaaS as CRaaS;
        
        $CRaaS = new CRaaS\WinnerService($this->user,$this->password);
        
        $WinnerResponse = $CRaaS->request([
                'method' => 'store'
            ],json_encode([
                "CampaignId"    => "12345",
                "Type"          => "CRaaS Competition",
                "EmailAddress"  => "joe.bloggs@website.com"
            ]));

        var_dump(json_decode($WinnerResponse->getBody(),true));
        
    ?>
    
Will output:

    string(36) "ebced309-d85e-43f3-9b4b-cdc4cce5c35a"

### Extending the Service class

The Service.php class provides methods to both validate the request parameters and use them to build the required request url.

To connect to a webservice other than CRaaS, create a new class and extend Heineken/WebService/Service.

    <?php
        namespace Heineken\WebService\CRaaS;
        use \Heineken\WebService\Service as Service;
    
        class PrizeService extends Service
        {
            protected $scope = "http://craas.heineken.com";
        
            protected $serviceUrl = "https://craas-v2.heineken.com/PrizeService.svc/v1/";
            
            protected $method = 'get';
        
            protected $methodsUrlExts = [
                "RedeemCode" => "redeem/{campaignId}/{code}",
                "ClaimPrize" => "claim/{campaignId}/{code}/{prizeId}"
            ];
        }
    ?>

Webservice methods are stored in the $methodsUrlExts (method url extensions) array, using the required method name as the array key. (e.g. "RedeemCode" or "ClaimPrize") The segments of the uri that are wrapped in {backets} will be validated and replaced against the parameters passed with the request. If a request doesn't contain a method or all the methods required variables, an "Exception" will be thrown.

In the above example "RedeemCode" requires both "campaignId" and "code" parameters.


