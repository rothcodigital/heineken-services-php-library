<?php namespace Heineken\Value;

use \Heineken\Value\ValueObject as ValueObject;

class Token extends ValueObject
{
    /**
     * Set the value.
     *
     * @param string $value
     * @return self
     */
    public function setValue($value)
    {
        return parent::setValue($value);
    }
}